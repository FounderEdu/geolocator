import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import '../template/globals.dart';


/// Example [Widget] showing the use of the Geocode plugin
class GeocodeWidget extends StatefulWidget {
  @override
  _GeocodeWidgetState createState() => _GeocodeWidgetState();
}

class _GeocodeWidgetState extends State<GeocodeWidget> {
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _latitudeController = TextEditingController();
  final TextEditingController _longitudeController = TextEditingController();
  String _output = '';
  // GeocodingIOS _geocodingIOS = GeocodingIOS();

  @override
  void initState() {
    _addressController.text = '上海市浦东新区青昆路755号';
    _latitudeController.text = '31.12';
    _longitudeController.text = '121.11';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: defaultHorizontalPadding + defaultVerticalPadding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.only(top: 32),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    autocorrect: false,
                    controller: _latitudeController,
                    style: Theme.of(context).textTheme.bodyMedium,
                    decoration: InputDecoration(
                      hintText: 'Latitude',
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: TextField(
                    autocorrect: false,
                    controller: _longitudeController,
                    style: Theme.of(context).textTheme.bodyMedium,
                    decoration: InputDecoration(
                      hintText: 'Longitude',
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ],
            ),
            const Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Center(
              child: ElevatedButton(
                  child: Text('Look up address'),
                  onPressed: () async {
                    print("========placemarkFromCoordinates===");
                    final latitude = double.parse(_latitudeController.text);
                    final longitude = double.parse(_longitudeController.text);
                    await placemarkFromCoordinates(latitude, longitude).then((placemarks) {
                      print("========placemarkFromCoordinates===返回数据==$placemarks");
                      var output = 'No results found.';
                      if (placemarks.isNotEmpty) {
                        output = placemarks[0].toString();
                      }

                      setState(() {
                        _output = output;
                      });
                    });
                  }),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 32),
            ),
            TextField(
              autocorrect: false,
              controller: _addressController,
              style: Theme.of(context).textTheme.bodyMedium,
              decoration: InputDecoration(
                hintText: 'Address',
              ),
              keyboardType: TextInputType.text,
            ),
            const Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Center(
              child: ElevatedButton(
                  child: Text('Look up location'),
                  onPressed: () async {
                    print("========locationFromAddress===");
                   await locationFromAddress(_addressController.text).then((locations) {
                     print("========locationFromAddress==返回数据=$locations");
                      var output = 'No results found.';
                      if (locations.isNotEmpty) {
                        output = locations[0].toString();
                      }

                      setState(() {
                        _output = output;
                      });
                    });
                  }),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Center(
                child: ElevatedButton(
                    child: Text('Is present'),
                    onPressed: () async {
                      print("========isPresent==");
                      await isPresent().then((isPresent) {
                        print("========isPresent==返回数据=$isPresent");
                        var output = isPresent
                            ? "Geocoder is present"
                            : "Geocoder is not present";
                        setState(() {
                          _output = output;
                        });
                      });
                    })),
            const Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Center(
                child: ElevatedButton(
                    child: Text('Set locale en_US'),
                    onPressed: () async {
                      await setLocaleIdentifier("en_US").then((_) {
                        setState(() {});
                      });
                    })),
            const Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Center(
                child: ElevatedButton(
                    child: Text('Set locale nl_NL'),
                    onPressed: () async {
                      await setLocaleIdentifier("nl_NL").then((_) {
                        setState(() {});
                      });
                    })),
            const Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(_output),
                ),
              ),
            )
          ],
        ));
  }
}
