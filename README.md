# geocoding

A new Flutter plugin project.

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter development, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

一。配置
1. 克隆库到工程文件夹，如：third，git clone https://gitee.com/FounderEdu/geolocator.git 
2. 下载下来的文件名为geolocator， 需要修改到geocoding, 在.yam添加引用
```
  geocoding:
      path: ./third/geocoding
```
3. 运行pub get

二。使用
1. 引入头文件 import 'package:geocoding/geocoding.dart';
```
import 'package:geocoding/geocoding.dart';
```
2. 在使用的文件内直接调用方法。

```
//通过地址，获取相应坐标
Future<List<Location>> locationFromAddress(String address,){}

//通过坐标获取相应地理位置信息
Future<List<Placemark>> placemarkFromCoordinates(double latitude, double longitude,) { }

```
