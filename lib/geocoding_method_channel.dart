
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:geocoding/src/errors/errors.dart';
import 'package:geocoding/src/models/models.dart';
import 'geocoding_platform_interface.dart';

// /// An implementation of [GeocodingPlatform] that uses method channels.

const MethodChannel _channel = MethodChannel('flutter.baseflow.com/geocoding');

class MethodChannelGeocoding extends GeocodingPlatform {

  /// Registers this class as the default instance of [GeocodingPlatform].
  // static void registerWith() {
  //   // print("==========registerWith");
  //   GeocodingPlatform.instance = MethodChannelGeocoding();
  // }

  @override
  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  String? _localeIdentifier;

  @override
  Future<void> setLocaleIdentifier(String localeIdentifier,) {
    // print("==========setLocaleIdentifier=$localeIdentifier");
    try {
      _localeIdentifier = localeIdentifier;

      final parameters = <String, String>{'localeIdentifier': localeIdentifier,};

      return _channel.invokeMethod('setLocaleIdentifier', parameters);
    } on PlatformException catch (e) {
      _handlePlatformException(e);
      rethrow;
    }
  }

  @override
  Future<List<Location>> locationFromAddress(
      String address,
      ) async {
    // print("==========locationFromAddress=$address");
    try {

      final parameters = <String, String>{'address': address,};
      final placeMarks = await _channel.invokeMethod('locationFromAddress', parameters,);

      return Location.fromOhosMaps(placeMarks);
    } on PlatformException catch (e) {
      _handlePlatformException(e);
      rethrow;
    }
  }

  @override
  Future<bool> isPresent() async {
    try {
      if(Platform.isIOS){
        return Future<bool>.value(true);
      }

      final isPresent = await _channel.invokeMethod('isPresent',);
      return isPresent;
    } on PlatformException catch (e) {
      _handlePlatformException(e);
      rethrow;
    }
  }


  @override
  Future<List<Placemark>> placemarkFromCoordinates(
      double latitude,
      double longitude,
      ) async {
    // print("====2====placemarkFromCoordinates=1");
    final parameters = <String, dynamic>{
      'latitude': latitude,
      'longitude': longitude,
    };

    final placeMarks = await _channel.invokeMethod('placemarkFromCoordinates', parameters);
    return Placemark.fromOhosMaps(placeMarks);
  }

  @override
  Future<List<Placemark>> placemarkFromAddress(String address,) async {
    // print("==========placemarkFromAddress=$address");
    final parameters = <String, String>{'address': address,};
    try {
      final placeMarks = await _channel.invokeMethod('placemarkFromAddress', parameters,);
      return Placemark.fromOhosMaps(placeMarks);
    } on PlatformException catch (e) {
      _handlePlatformException(e);
      rethrow;
    }
  }

  void _handlePlatformException(PlatformException platformException) {
    switch (platformException.code) {
      case 'NOT_FOUND':
        throw const NoResultFoundException();
    }
  }
}