import 'package:flutter_test/flutter_test.dart';
import 'package:geocoding/geocoding_platform_interface.dart';
import 'package:geocoding/geocoding_method_channel.dart';
import 'package:geocoding/src/models/location.dart';
import 'package:geocoding/src/models/placemark.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockGeocodingPlatform
    with MockPlatformInterfaceMixin
    implements GeocodingPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<bool> isPresent() {
    // TODO: implement isPresent
    throw UnimplementedError();
  }

  @override
  Future<List<Location>> locationFromAddress(String address) {
    // TODO: implement locationFromAddress
    throw UnimplementedError();
  }

  @override
  Future<List<Placemark>> placemarkFromAddress(String address) {
    // TODO: implement placemarkFromAddress
    throw UnimplementedError();
  }

  @override
  Future<List<Placemark>> placemarkFromCoordinates(double latitude, double longitude) {
    // TODO: implement placemarkFromCoordinates
    throw UnimplementedError();
  }

  @override
  Future<void> setLocaleIdentifier(String localeIdentifier) {
    // TODO: implement setLocaleIdentifier
    throw UnimplementedError();
  }
}

void main() {
  final GeocodingPlatform initialPlatform = GeocodingPlatform.instance;

  test('$MethodChannelGeocoding is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelGeocoding>());
  });

  test('getPlatformVersion', () async {
    // Geocoding geocodingPlugin = Geocoding();
    MockGeocodingPlatform fakePlatform = MockGeocodingPlatform();
    GeocodingPlatform.instance = fakePlatform;

    // expect(await geocodingPlugin.getPlatformVersion(), '42');
  });
}
